@extends('layouts.blank')
@section('title', 'network')
@section('content')
    <form action="{{ route('login') }}" method="post">
        @csrf

        <div>
            <input type="email" name="email" placeholder="{{ trans('common.email') }}">
        </div>
        <div>
            <input type="password" name="password" placeholder="{{ trans('common.password') }}">
        </div>
        <button type="submit">{{ trans('common.login') }}</button>
    </form>
@endsection