<?php

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@login']);
        Route::post('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });

    Route::get('home', ['uses' => 'HomeController@home', 'as' => 'home']);
});
