<?php

namespace Tests\Feature;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function test_show_landing_page()
    {
        $response = $this->get(route('index'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_access_to_the_user_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('home'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_access_to_the_user_dashboard_fails()
    {
        $response = $this->get(route('home'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
